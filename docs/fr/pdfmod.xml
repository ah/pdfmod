<?xml version="1.0" encoding="utf-8"?>
<?db.chunk.max_depth 1?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML 4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<book id="pdfmod" lang="fr">
	<bookinfo>
		<title>Manuel de PDF Mod</title>
		<abstract role="description">
			<para>Ceci est le manuel des utilisateurs de PDF Mod, un outil simple pour manipuler les documents PDF.</para>
		</abstract>
		<subtitle>Modifications rapides et faciles de fichiers PDF.</subtitle>
		<copyright>
			<year>2009</year>
			<holder>Novell, Inc.</holder>
		</copyright><copyright><year>2009</year><holder>Olivier Lê Thanh Duong (olivier@lethanh.be)</holder></copyright><copyright><year>2010</year><holder>Bruno Brouard (annoa.b@gmail.com)</holder></copyright>
		<publisher>
			<publishername>Projet de documentation GNOME</publishername>
		</publisher>
		<authorgroup>
			<author role="maintainer">
				<firstname>Gabriel</firstname>
				<surname>Burt</surname>
				<affiliation>
					<orgname>Novell, Inc.</orgname>
				</affiliation>
			</author>
		</authorgroup>
		<revhistory>
			<revision>
				<revnumber>0.2</revnumber>
				<date>29/07/2009</date>
			</revision>
			<revision>
				<revnumber>0.1</revnumber>
				<date>05/07/2009</date>
			</revision>
		 </revhistory>
	</bookinfo>

	<chapter id="introduction">
		<title>Introduction</title>
			<para>PDF Mod est un outil simple pour effectuer des manipulations basiques de documents PDF.</para>

			<para>Avec cet outil, vous pouvez tourner, supprimer ou extraire des pages d'un document PDF. Vous pouvez aussi ajouter des pages provenant d'un autre document.</para>

			<para>Vous pouvez démarrer PDF Mod depuis le menu Applications ou en faisant un clic droit sur un ou plusieurs documents PDF dans votre navigateur de fichiers et en choisissant de les ouvrir avec PDF Mod.</para>

			<para>Comme pour toutes applications d'édition, vous devez enregistrer les modifications effectuées avec PDF Mod. Si vous souhaitez conserver votre document d'origine et enregistrer vos modifications dans un nouveau fichier, utilisez le menu « Enregistrer sous ».</para>
	</chapter>

	<chapter id="usage">
		<title>Utilisation</title>

		<sect1 id="opening-file">
			<title>Ouverture d'un document</title>
			<para>Pour ouvrir un document dans PDF Mod :</para>
			<itemizedlist>
			  <listitem>
				  <para>choisissez <menuchoice><guimenu>Fichier</guimenu><guimenuitem>Ouvrir</guimenuitem></menuchoice> et sélectionnez votre document ou</para>
			  </listitem>
			  <listitem>
				  <para>pressez <keycombo><keycap>Ctrl</keycap><keycap>O</keycap></keycombo> et sélectionnez votre document ou</para>
			  </listitem>
			  <listitem>
				  <para>faites glisser un document PDF depuis votre bureau ou votre navigateur de fichiers sur une fenêtre de PDF Mod qui ne contient pas déjà un document ouvert ou</para>
			  </listitem>
			  <listitem>
				  <para>choisissez un document depuis <menuchoice><guimenu>Fichier</guimenu><guimenuitem>Fichiers récents</guimenuitem></menuchoice>.</para>
			  </listitem>
		  </itemizedlist>
		</sect1>

		<sect1 id="zooming">
			<title>Zoom</title>
			<para>Effectuer un zoom pour agrandir ou diminuer les miniatures des pages. PDF Mod démarre dans le mode « Ajuster au mieux » dans lequel il essaye d'afficher toutes les pages à la fois.</para>
			<para>Vous pouvez effectuer un zoom avant ou arrière en utilisant le menu <menuchoice><guimenu>Affichage</guimenu></menuchoice> ou en maintenant appuyée la touche <keycap>Ctrl</keycap> et en utilisant la molette de votre souris.</para>
		</sect1>

		<sect1 id="properties">
			<title>Affichage et modification des propriétés</title>
			<para>Vous pouvez afficher et modifier le titre, l'auteur, les mots-clefs et le sujet du document en affichant les propriétés. Pour cela choisissez <menuchoice><guimenu>Fichier</guimenu><guimenuitem>Propriétés</guimenuitem></menuchoice>, pressez <keycombo><keycap>Alt</keycap><keycap>Entrée</keycap></keycombo> ou cliquez sur le bouton « Propriétés » dans la barre d'outils.</para>
		</sect1>

		<sect1 id="selection">
			<title>Sélection des pages</title>
			<para>PDF Mod peut sélectionner automatiquement toutes les pages, les pages paires ou impaires ou les pages contenant un terme recherché. Ces commandes sont disponibles dans le menu <menuchoice><guimenu>Édition</guimenu></menuchoice>.</para>
			<para>Vous pouvez également sélectionner des pages manuellement en utilisant le clavier et la souris. Utilisez <keycap>Ctrl</keycap> ou <keycap>Maj</keycap> pour sélectionner plus d'une page.</para>
		</sect1>

		<sect1 id="moving-pages">
			<title>Déplacement des pages</title>
			<para>Pour déplacer (ou réordonner) la ou les pages sélectionnées, faites les glisser à la position désirée dans le document.</para>
			<tip>
				<para>Toutes les actions d'édition, sauf la suppression des pages, peuvent être annulées en choisissant <menuchoice><guimenu>Édition</guimenu><guimenuitem>Annuler</guimenuitem></menuchoice> ou en pressant <keycombo><keycap>Ctrl</keycap><keycap>Z</keycap></keycombo>.</para>
			</tip> 
		</sect1>

		<sect1 id="extracting-pages">
			<title>Extraction des pages</title>
			<para>L'extraction de ou des pages sélectionnées ouvre une nouvelle fenêtre PDF Mod contenant uniquement les pages sélectionnées dans un nouveau document, prêtes à être modifiées ou enregistrées.</para>
			<para>Pour extraire la ou les pages sélectionnées, choisissez  <menuchoice><guimenu>Édition</guimenu><guimenuitem>Extraire la (les) page(s)</guimenuitem></menuchoice>.</para>
			<tip>
				<para>Toutes les actions d'édition ou de sélection disponibles dans le menu <menuchoice><guimenu>Édition</guimenu></menuchoice> sont également disponibles par un clic droit sur une page. Certaines actions sont également disponibles dans la barre d'outils.</para>
			</tip> 
		</sect1>

		<sect1 id="rotating-pages">
			<title>Rotation des pages</title>
			<para>Pour tourner la ou les pages sélectionnées, choisissez <menuchoice><guimenu>Édition</guimenu><guimenuitem>Tourner</guimenuitem></menuchoice> ou appuyez sur <keycombo><keycap>[</keycap></keycombo> pour effectuer une rotation vers la gauche (sens inverse des aiguilles) et sur <keycombo><keycap>]</keycap></keycombo> pour effectuer une rotation vers la droite (sens des aiguilles).</para>
		</sect1>

		<sect1 id="removing-pages">
			<title>Suppression des pages</title>
			<para>Pour supprimer la ou les pages sélectionnées, appuyez sur la touche <keycap>Suppr</keycap> ou choisissez <menuchoice><guimenu>Édition</guimenu><guimenuitem>Supprimer la page</guimenuitem></menuchoice>.</para>
			<warning>
				<para>Il n'est pour l'instant pas possible d'annuler cette action à l'aide du menu <menuchoice><guimenu>Édition</guimenu><guimenuitem>Annuler</guimenuitem></menuchoice>. Vous pouvez fermer le document sans enregistrer et l'ouvrir à nouveau pour récupérer votre page mais vous perdrez toutes les autres modifications que vous auriez déjà effectuées.</para>
			</warning> 
		</sect1>

		<sect1 id="saving">
			<title>Enregistrement</title>
			<para>Après avoir modifié le document, il y a deux façons d'enregistrer votre travail. Vous pouvez écraser le document original en choisissant <menuchoice><guimenu>Fichier</guimenu><guimenuitem>Enregistrer</guimenuitem></menuchoice> ou vous pouvez enregistrer vos modifications dans un nouveau fichier en choisissant <menuchoice><guimenu>Fichier</guimenu><guimenuitem>Enregistrer sous</guimenuitem></menuchoice>.</para>
		</sect1>

	</chapter>
	
</book>
