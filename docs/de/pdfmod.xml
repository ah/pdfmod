<?xml version="1.0" encoding="utf-8"?>
<?db.chunk.max_depth 1?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML 4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<book id="pdfmod" lang="de">
	<bookinfo>
		<title>Handbuch für PDF Mod</title>
		<abstract role="description">
			<para>Dies ist das Handbuch für <application>PDF Mod</application>, einem einfachen Werkzeug zur Bearbeitung von PDF-Dokumenten.</para>
		</abstract>
		<subtitle>Schnelle, einfache PDF-Bearbeitung</subtitle>
		<copyright>
			<year>2009</year>
			<holder>Novell, Inc.</holder>
		</copyright><copyright><year>2009</year><holder>Benjamin Elbers (elbersb@gmail.com)</holder></copyright><copyright><year>2009</year><holder>Mario Blättermann (mariobl@gnome.org)</holder></copyright>
		<publisher>
			<publishername>GNOME-Dokumentationsprojekt</publishername>
		</publisher>
		<authorgroup>
			<author role="maintainer">
				<firstname>Gabriel</firstname>
				<surname>Burt</surname>
				<affiliation>
					<orgname>Novell, Inc.</orgname>
				</affiliation>
			</author>
		</authorgroup>
		<revhistory>
			<revision>
				<revnumber>0.2</revnumber>
				<date>29. Juli 2009</date>
			</revision>
			<revision>
				<revnumber>0.1</revnumber>
				<date>25. Juli 2009</date>
			</revision>
		 </revhistory>
	</bookinfo>

	<chapter id="introduction">
		<title>Einführung</title>
			<para><application>PDF Mod</application> ist ein einfaches Werkzeug für die grundlegende Bearbeitung von PDF-Dokumenten.</para>

			<para>Mit diesem Programm können Sie die Seiten eines PDF-Dokuments drehen oder entfernen, und auch Seiten extrahieren. Es ist auch möglich, Seiten aus anderen PDF-Dokumenten hinzuzufügen.</para>

			<para>Sie können <application>PDF-Mod</application> aus dem Menü <guimenu>Anwendungen</guimenu> oder durch einen Starter aufrufen. Alternativ klicken Sie mit der rechten Maustaste auf eines oder mehrere PDF-Dokumente im Dateimanager und wählen <guilabel>Mit »PDF Mod« öffnen</guilabel>.</para>

			<para>Wie in jedem normalen Bearbeitungsprogramm müssen Veränderungen gespeichert werden, die mit <application>PDF Mod</application> durchgeführt wurden. Wenn Sie das Original-Dokument behalten wollen und die Veränderungen in eine neue Datei speichern wollen, benutzen Sie bitte <guilabel>Speichern unter</guilabel>.</para>
	</chapter>

	<chapter id="usage">
		<title>Anwendung</title>

		<sect1 id="opening-file">
			<title>Ein Dokument öffnen</title>
			<para>So öffnen Sie ein Dokument in <application>PDF Mod</application>:</para>
			<itemizedlist>
			  <listitem>
				  <para>Wählen Sie <menuchoice><guimenu>Datei</guimenu><guimenuitem>Öffnen</guimenuitem></menuchoice> und wählen Sie die Datei aus, oder</para>
			  </listitem>
			  <listitem>
				  <para>drücken Sie <keycombo><keycap>Strg</keycap><keycap>O</keycap></keycombo> und wählen Sie die Datei aus, oder</para>
			  </listitem>
			  <listitem>
				  <para>ziehen Sie ein PDF-Dokument von der Arbeitsfläche oder aus dem Datei-Manager in ein <application>PDF-Mod</application>-Fenster, in dem noch kein Dokument geöffnet ist, oder</para>
			  </listitem>
			  <listitem>
				  <para>wählen Sie ein Dokument aus <menuchoice><guimenu>Datei</guimenu><guimenuitem>Zuletzt bearbeitete Dokumente</guimenuitem></menuchoice>.</para>
			  </listitem>
		  </itemizedlist>
		</sect1>

		<sect1 id="zooming">
			<title>Größe anpassen</title>
			<para>Dadurch wird die Größe der Vorschaubilder der PDF-Seiten verändert. <application>PDF Mod</application> versucht so zu starten, dass alle Seiten auf einmal angezeigt werden.</para>
			<para>Sie können die Ansicht mit den Optionen unter <menuchoice><guimenu>Ansicht</guimenu></menuchoice> vergrößern oder verkleinern, oder Sie drücken <keycap>Strg</keycap> und bewegen das Scrollrad ihrer Maus.</para>
		</sect1>

		<sect1 id="properties">
			<title>Betrachten und Verändern von Eigenschaften</title>
			<para>Sie können den Titel, den Autor, die Schlüsselwörter und den Titel des Dokuments betrachten und verändern, indem Sie <menuchoice><guimenu>Datei</guimenu><guimenuitem>Eigenschaften</guimenuitem></menuchoice> wählen, <keycombo><keycap>Alt</keycap><keycap>Enter</keycap></keycombo> drücken oder auf den <guibutton>Eigenschaften</guibutton>-Knopf in der Werkzeugleiste klicken.</para>
		</sect1>

		<sect1 id="selection">
			<title>Seiten auswählen</title>
			<para><application>PDF Mod</application> kann automatisch alle Seiten oder Seiten mit gerader oder ungerader Seitennummer auswählen. Sie können auch Seiten mit einem passenden Suchtext auswählen. Diese Optionen finden Sie unter <menuchoice><guimenu>Bearbeiten</guimenu></menuchoice>.</para>
			<para>Sie können die Seiten auch manuell per Tastatur oder Maus auswählen. Benutzen Sie <keycap>Strg</keycap> oder die <keycap>Umschalttaste</keycap>, um mehr als eine Seite auszuwählen.</para>
		</sect1>

		<sect1 id="moving-pages">
			<title>Reihenfolge der Seiten verändern</title>
			<para>Um Seiten neu anzuordnen, ziehen Sie sie an die Position, die die Seiten im Dokument einnehmen sollen.</para>
			<tip>
				<para>Alle Aktionen außer dem Entfernen von Seiten können rückgängig gemacht werden, indem Sie <menuchoice><guimenu>Bearbeiten</guimenu><guimenuitem>Rückgängig</guimenuitem></menuchoice> wählen oder <keycombo><keycap>Strg</keycap><keycap>Z</keycap></keycombo> drücken.</para>
			</tip> 
		</sect1>

		<sect1 id="extracting-pages">
			<title>Seiten extrahieren</title>
			<para>Wenn Sie die ausgewählten Seiten extrahieren, öffnet sich ein neues <application>PDF-Mod</application>-Fenster mit nur diesen Seiten, um weiter bearbeitet oder gespeichert zu werden.</para>
			<para>Um die ausgewählten Seiten zu extrahieren, wählen Sie <menuchoice><guimenu>Bearbeiten</guimenu><guimenuitem>Seite extrahieren</guimenuitem></menuchoice>.</para>
			<tip>
				<para>Alle Bearbeitungs- und Auswahlaktionen, die unter <menuchoice><guimenu>Bearbeiten</guimenu></menuchoice> zur Verfügung stehen, können auch durch einen Rechtsklick auf eine Seite aktiviert werden. Einige sind auch in der Werkzeugleiste verfügbar.</para>
			</tip> 
		</sect1>

		<sect1 id="rotating-pages">
			<title>Seiten drehen</title>
			<para>Um die ausgewählten Seiten zu drehen, wählen Sie <menuchoice><guimenu>Bearbeiten</guimenu><guimenuitem>Seite drehen</guimenuitem></menuchoice> oder drücken Sie <keycombo><keycap>[</keycap></keycombo>, um nach links zu drehen (gegen den Uhrzeigersinn) und <keycombo><keycap>]</keycap></keycombo> um nach rechts zu drehen (im Uhrzeigersinn).</para>
		</sect1>

		<sect1 id="removing-pages">
			<title>Seiten entfernen</title>
			<para>Um ausgewählte Seiten zu entfernen, drücken Sie <keycap>Entf</keycap> oder wählen Sie <menuchoice><guimenu>Bearbeiten</guimenu><guimenuitem>Seite entfernen</guimenuitem></menuchoice>.</para>
			<warning>
				<para>Es ist momentan nicht möglich, diese Aktion mit <menuchoice><guimenu>Bearbeiten</guimenu><guimenuitem>Rückgängig</guimenuitem></menuchoice> zu widerrufen. Sie können das Dokument aber schließen, ohne zu speichern und erneut öffnen. Dabei gehen allerdings alle anderen Änderungen verloren.</para>
			</warning> 
		</sect1>

		<sect1 id="saving">
			<title>Speichern</title>
			<para>Nachdem Sie Veränderungen vorgenommen haben, gibt es zwei Wege, um Ihre Arbeit zu speichern. Sie können das bisherige Dokument mit <menuchoice><guimenu>Datei</guimenu><guimenuitem>Speichern</guimenuitem></menuchoice> überschreiben oder Sie speichern mit  <menuchoice><guimenu>Datei</guimenu><guimenuitem>Speichern unter</guimenuitem></menuchoice> in einer neuen Datei.</para>
		</sect1>

	</chapter>
	
</book>
